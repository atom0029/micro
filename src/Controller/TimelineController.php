<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Timeline Controller
 *
 * @method \App\Model\Entity\Timeline[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TimelineController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        // $timeline = $this->paginate($this->Timeline);

        $this->viewBuilder()->setLayout('ajax');
    }

    /**
     * View method
     *
     * @param string|null $id Timeline id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $timeline = $this->Timeline->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('timeline'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $timeline = $this->Timeline->newEmptyEntity();
        if ($this->request->is('post')) {
            $timeline = $this->Timeline->patchEntity($timeline, $this->request->getData());
            if ($this->Timeline->save($timeline)) {
                $this->Flash->success(__('The timeline has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The timeline could not be saved. Please, try again.'));
        }
        $this->set(compact('timeline'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Timeline id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $timeline = $this->Timeline->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $timeline = $this->Timeline->patchEntity($timeline, $this->request->getData());
            if ($this->Timeline->save($timeline)) {
                $this->Flash->success(__('The timeline has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The timeline could not be saved. Please, try again.'));
        }
        $this->set(compact('timeline'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Timeline id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $timeline = $this->Timeline->get($id);
        if ($this->Timeline->delete($timeline)) {
            $this->Flash->success(__('The timeline has been deleted.'));
        } else {
            $this->Flash->error(__('The timeline could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
