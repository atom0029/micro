<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Mailer\Mailer;
use Cake\ORM\Query;
use Cake\Utility\Security;
use Cake\I18n\FrozenTime;
use Cake\Validation\Validator;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['login']);
    }

    public function login()
    {
        $user = $this->Users->newEmptyEntity();
        $this->set('user', $user);
        $this->set('pageTitle', 'Login');
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            // redirect to /articles after login success
            $redirect = $this->request->getQuery('redirect', [
                'controller' => 'Posts',
                'action' => 'index',
            ]);

            return $this->redirect($redirect);
        }
        // display error if user submitted and authentication failed
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'));
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('user'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function register()
    {
        $this->set('pageTitle', 'Register');
        $this->loadModel('Validations');
        $validation = $this->Validations->newEmptyEntity();
        $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            
            $user->first_name =  $this->request->getData('first_name');
            $user->middle_name =  $this->request->getData('middle_name');
            $user->last_name =  $this->request->getData('last_name');
            $user->username =  $this->request->getData('username');
            $user->password =  $this->request->getData('password');
            $user->email =  $this->request->getData('email');
            if ($this->Users->save($user)) {
                $validation->user_id = $user->id;
                $validation->validation_key = Security::hash(Security::randomBytes(32));
                if ($this->Validations->save($validation)) {
                    $mailer = new Mailer();
                    $mailer
                        ->setEmailFormat('html')
                        ->setTo($this->request->getData('email'))
                        ->setFrom('app@domain.com')
                        ->setViewVars(['key' => $validation->validation_key])
                        ->viewBuilder()
                            ->setTemplate('verify');

                    $mailer->deliver();
                    $this->Flash->success(__('Registration Success. Please verify your email.'));
                    return $this->redirect(['action' => 'login/']);
                }
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set('user', $user);

    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set('user',$user);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Verify method
     *
     * @param string|null $key validation_key.
     * @return \Cake\Http\Response|null|void Redirects to login with message account activated.
     * @return \Cake\Http\Response|null|void Redirects to login with message activation key invalid.
     */
    public function verify($key = null)
    {
        $this->loadModel('Validations');
        $validation = $this->Validations->find('all')->where(['validation_key' => $key])->where(['deleted is' =>  NULL])->first();
        $results = $validation->toArray();
        if (sizeof($results) > 0) {
            $user = $this->Users->get($results['user_id']);
            $user->verified = FrozenTime::now();
            if($this->Users->save($user)){
                $this->request->allowMethod(['get', 'delete']);
                $this->Validations->delete($validation);
                $this->Flash->success(__('This Account has been activated.'));
                return $this->redirect(['action' => 'login']);
            }
        }
        else{
            $this->Flash->error(__('This activation link is invalid.'));
            return $this->redirect(['action' => 'login']);
        }
        
    }

     /**
     * logout method
     *
     * @return \Cake\Http\Response|null|void Redirects to login.
     */
    public function logout()
    {
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }
}