<!--
=========================================================
* Argon Dashboard - v1.2.0
=========================================================
* Product Page: https://www.creative-tim.com/product/argon-dashboard

* Copyright  Creative Tim (http://www.creative-tim.com)
* Coded by www.creative-tim.com
=========================================================
* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  

  <title>
       <?= $pageTitle; ?>
  </title>
  <!-- Favicon -->
  <link rel="icon" href="https://cdn.iconscout.com/icon/free/png-256/m-character-alphabet-letter-32858.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <?= $this->Html->css('/assets/vendor/nucleo/css/nucleo.css',array('inline' => false)); ?>
  <?= $this->Html->css('/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css',array('inline' => false)); ?>
  <!-- Argon CSS -->
  <?= $this->Html->css('/assets/css/argon.css?v=1.2.0',array('inline' => false)); ?>
  <style type="text/css">
  .input-group .form-control {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0;
}
  </style>
</head>

<body class="bg-default">
   
    
    <?= $this->fetch('content') ?>

     <footer class="py-5" id="footer-main">
        <div class="container">
          <div class="row align-items-center justify-content-xl-between">
            <div class="col-xl-6">
              <div class="copyright text-center text-xl-left text-muted">
                &copy; 2022 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Microblog</a>
              </div>
            </div>
          
          </div>
        </div>
      </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <?= $this->Html->script('/assets/vendor/jquery/dist/jquery.min.js',array('inline' => false)); ?>
  <?= $this->Html->script('/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js',array('inline' => false)); ?>
  <?= $this->Html->script('/assets/vendor/js-cookie/js.cookie.js',array('inline' => false)); ?>
  <?= $this->Html->script('/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js',array('inline' => false)); ?>
  <?= $this->Html->script('/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js',array('inline' => false)); ?>
  <?= $this->Html->script('/assets/vendor/@fortawesome/fontawesome-free/js/regular.min.js',array('inline' => false)); ?>
 
  <!-- Argon JS -->
  <?= $this->Html->script('/assets/js/argon.js?v=1.2.0',array('inline' => false)); ?>
  
</body>

</html>
