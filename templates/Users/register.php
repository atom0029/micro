<!-- Main content -->
<div class="main-content">
<!-- Header -->
<div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
  <div class="container">
    <div class="header-body text-center mb-7">
      <div class="row justify-content-center">
        <div class="col-xl-5 col-lg-6 col-md-8 px-5">
          <h1 class="text-white">Create an account</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="separator separator-bottom separator-skew zindex-100">
    <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
      <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
    </svg>
  </div>
</div>
<!-- Page content -->
<div class="container mt--9 pb-5">
  <!-- Table -->
  <div class="row justify-content-center">
    <div class="col-lg-6 col-md-8">
      <div class="card bg-secondary border-0">
        
        <div class="card-body px-lg-5 py-lg-5">
          <div class="text-center text-muted mb-4">
            <small>Sign up with credentials</small>
          </div>
          <div class="text-muted text-center mb-2"><?= $this->Flash->render() ?></div>
          <?= $this->Form->create($user) ?>
            <input type="hidden" name="_csrfToken" value="<?= $this->request->getAttribute('csrfToken')?>">
            <?= $this->Form->control('first_name',['label' => false, 'class' => 'form-control , mb-3','placeholder'=> 'First Name']);?>
            <?= $this->Form->control('middle_name',['label' => false, 'class' => 'form-control , mb-3','placeholder'=> 'Middle Name']);?>
            <?= $this->Form->control('last_name',['label' => false, 'class' => 'form-control , mb-3','placeholder'=> 'Last Name']);?>
            <?= $this->Form->control('username',['label' => false, 'class' => 'form-control , mb-3','placeholder'=> 'Username']);?>
            <?= $this->Form->control('email',['label' => false, 'class' => 'form-control , mb-3','placeholder'=> 'Email']);?>
            <?= $this->Form->control('password',['label' => false, 'type' => 'password' , 'class' => 'form-control , mb-3','placeholder'=> 'Password']);?>
            <?= $this->Form->control('confirmPassword',['label' => false, 'type' => 'password' , 'class' => 'form-control , mb-3','placeholder'=> 'Confirm Password']);?>
            
            <div class="text-center">
                <button type="submit" class="btn btn-primary mt-4">Create account</button>
            </div>
            
          <?= $this->Form->end() ?>
        <div class="text-center mt-3">
            <a href="login">Login</a>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
