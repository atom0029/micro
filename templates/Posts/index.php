<?= $this->Html->css('post.css',array('inline' => false)); ?>
<div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <img src="https://www.microlog.it/wp-content/uploads/2019/05/microblog-verde-01.png" style="max-width: 40vh; max-height: 5vh" class="mr-3">
          <!-- Search form -->
          <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
            <div class="form-group mb-0">
              <div class="input-group input-group-alternative input-group-merge">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <input class="form-control" placeholder="Search" type="text">
              </div>
            </div>
            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </form>
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center  ml-md-auto ">
            <li class="nav-item d-xl-none">
              <!-- Sidenav toggler -->
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li>
            
           
          </ul>
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="assets/img/theme/team-4.jpg">
                  </span>
                  <div class="media-body  ml-2  d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold"><?= $_SESSION['Auth']['first_name'] ?></span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Welcome!</h6>
                </div>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span>My profile</span>
                </a>
                <a href="#!" class="dropdown-item">
                  <i class="ni ni-settings-gear-65"></i>
                  <span>Settings</span>
                </a>
                
                <div class="dropdown-divider"></div>
                <a href="logout" class="dropdown-item">
                  <i class="ni ni-user-run"></i>
                  <span>Logout</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
   <div class="d-flex align-content-stretch flex-wrap">...</div>



    <div class="container-fluid mt-2">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Create Post</button>
      <div class="text-muted text-center mb-2" style="color: black; background-color: white"><?= $this->Flash->render() ?></div>
        <?php foreach ($posts as $key => $post): ?>
          <div class="row">
              <!-- BLOGPOST -->
              <div class="col-xl-8 order-xl-2" >
                  <input type="hidden" name="postId" value="<?= $post['id']?>">
                  <div class="card card-profile">
                      <div class="p-2">
                          <div class="d-flex flex-row justify-content-between align-items-start profile">
                              <div class="d-flex align-items-center">
                                  <img class="rounded-circle img-responsive" src="https://i.imgur.com/44HzzUN.jpg" width="50" height="50">
                                  <div class="d-flex flex-column ml-2">
                                      <h5 class="h4 mr-2">
                                        <i class="ni location_pin mr-2"></i><?= $post['user']['first_name'] . ' ' . $post['user']['last_name'] ?><span class="font-weight-light"> (<?= $post['user']['username'] ?>) </span>
                                      </h5>
                                      <div class="h5 font-weight-300 mt--2">
                                        <i class="ni location_pin mr-2"></i>><?= $post['created'] ?>
                                      </div>
                                  </div>
                              </div>
                              <i class="fa fa-ellipsis-h text-black-50 mt-2"></i>
                          </div>
                          <div class="profile-content mt-3 ml-2">
                              <span><?= $post['post_description'] ?></span>
                              <?php if($post['post_image'] != NULL):  $url = 'http://localhost/microblog/webroot/img/' . $post['post_image']  ?>
                              <div class="content-image"  style="height: 60vh">
                                  <img style='height: 100%; width: 100%;'  src="<?= $url ?>">
                              </div>
                            <?php endif; ?>
                          </div>
                          <div class="profile-engagements"></div>
                      </div>
                  </div>
              </div>
              <?php if($key == 0): ?>
              <!-- BLOGNAV -->
              <div class="col-xl-4 order-xl-2">
                <div class="card card-profile">
                  <img src="../assets/img/theme/img-1-1000x600.jpg" alt="Image placeholder" class="card-img-top">
                  <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                      <div class="card-profile-image">
                        <a href="#">
                          <img src="../assets/img/theme/team-4.jpg" class="rounded-circle">
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div class="d-flex justify-content-between">
                      <a href="#" class="btn btn-sm btn-info  mr-4 ">Connect</a>
                      <a href="#" class="btn btn-sm btn-default float-right">Message</a>
                    </div>
                  </div>
                  <div class="card-body pt-0">
                    <div class="row">
                      <div class="col">
                        <div class="card-profile-stats d-flex justify-content-center">
                          <div>
                            <span class="heading">22</span>
                            <span class="description">Friends</span>
                          </div>
                          <div>
                            <span class="heading">10</span>
                            <span class="description">Photos</span>
                          </div>
                          <div>
                            <span class="heading">89</span>
                            <span class="description">Comments</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <h5 class="h3">
                        Jessica Jones<span class="font-weight-light">, 27</span>
                      </h5>
                      <div class="h5 font-weight-300">
                        <i class="ni location_pin mr-2"></i>Bucharest, Romania
                      </div>
                      <div class="h5 mt-4">
                        <i class="ni business_briefcase-24 mr-2"></i>Solution Manager - Creative Tim Officer
                      </div>
                      <div>
                        <i class="ni education_hat mr-2"></i>University of Computer Science
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php endif; ?>
          </div>

        <?php endforeach; ?>

          
    </div>

    

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="container mx-auto">
                    <div class="float-right" data-dismiss="modal" style="cursor:pointer;">&#10006;</div>
                    <div class="row d-flex justify-content-center">
                        <div class="cardPost">
                          <?= $this->Form->create(null, ['type' => 'file','url' => [ 'controller' => 'Posts', 'action' => 'add']]); ?>
                            <input type="hidden" name="_csrfToken" value="<?= $this->request->getAttribute('csrfToken')?>">
                            <input type="hidden" name="user_id" value="<?= $_SESSION["Auth"]["id"]?>">
                            <div class="row px-3 form-group"> 
                                <?= $this->Form->textarea('post_description',['label' => false, 'class' => 'bg-light mt-4 mb-3','placeholder'=> 'Hi '.$_SESSION["Auth"]["first_name"].', whats on your mind today?']);?>
                            </div>
                            <div class="row px-3">
                                <input type="file" id="upload" name="upload" accept="image/png, image/jpeg">
                                <!-- <p class="fa fa-image options mb-0 mr-4"></p> -->
                                <button class="btn btn-success ml-auto" type="submit">Post</button>
                            </div>
                          <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>