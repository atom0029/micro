<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ValidationsFixture
 */
class ValidationsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'validation_key' => 'Lorem ipsum dolor sit amet',
                'created' => '2022-01-21 07:44:53',
                'modified' => '2022-01-21 07:44:53',
                'deleted' => '2022-01-21 07:44:53',
            ],
        ];
        parent::init();
    }
}
