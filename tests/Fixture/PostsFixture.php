<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PostsFixture
 */
class PostsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'post_description' => 'Lorem ipsum dolor sit amet',
                'post_image' => 'Lorem ipsum dolor sit amet',
                'post_shared' => 1,
                'created' => '2022-01-23 22:11:07',
                'modified' => '2022-01-23 22:11:07',
                'deleted' => '2022-01-23 22:11:07',
            ],
        ];
        parent::init();
    }
}
