<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PostFixture
 */
class PostFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'post';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'post_description' => 'Lorem ipsum dolor sit amet',
                'post_image' => 'Lorem ipsum dolor sit amet',
                'post_shared_id' => 1,
                'created' => '2022-01-23 21:36:41',
                'modified' => '2022-01-23 21:36:41',
                'deleted' => '2022-01-23 21:36:41',
            ],
        ];
        parent::init();
    }
}
